# Примеры работы с Grunt/Gulp

## Gulp

Пример работы с gulp, bower, uncss.

1. Переходим в папку gulp/
1. `npm i && bower i` — устанавливаем npm-зависимости и bootstrap.css. В папке dist/ есть два html-файла, которые используют только часть bootstrap.css. Задача: убрать из bootstrap.css стили, которые не используются.
1. Запускаем `gulp build`: плагин uncss удаляет неиспользуемые стили, плагин htmlreplace подключает в link'ах уменьшенный css
1. Результат dest/main.html и dest/about.html

## Grunt

Пример работы с grunt, browserSync, postСss.

1. Переходим в папку grunt/
1. `npm i` устанавливаем npm-зависимости.
1. Пример компонента: app/components/auth/
1. Запускаем `grunt serve`, ждем пока откроется браузер. Пока скомпилированного CSS нет, видна только разметка.
1. Меняем что-то в app/components/auth/auth.scss — auth.scss будет собран с другими компонентами в папке components в build.scss, который потом будет скомпилирован в app/css/all.css. Верстка обновится в браузере без перезагрузки.
