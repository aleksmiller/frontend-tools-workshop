var gulp = require('gulp');
var runSequence = require('run-sequence');
var htmlreplace = require('gulp-html-replace');
var uncss = require('gulp-uncss');
var processhtmlOpts = {};

// `gulp styles`
// remove unused css from bootstrap
gulp.task('styles', function() {
  gulp.src('dist/components/bootstrap/dist/css/bootstrap.css')
    .pipe(uncss({
      html: ['dist/*.html']
    }))
    .pipe(gulp.dest('./dest/css'));
});

// `gulp paths`
// replace paths to cleaned css in html files
gulp.task('paths', function () {
  gulp.src('./dist/*.html')
   .pipe(htmlreplace({
     'css': 'css/bootstrap.css'
   }))
   .pipe(gulp.dest('./dest/'));
});

// `gulp build`
gulp.task('build', function(done) {
  runSequence('styles', 'paths', done);
});
