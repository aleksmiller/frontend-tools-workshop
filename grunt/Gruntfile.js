module.exports = function (grunt) {
    grunt.initConfig({
        watch: {
            styles: {
                files: 'app/**/*.scss',
                tasks: ['concat', 'sass', 'postcss']
            }
        },
        concat: {
            dist: {
                src: [
                    'app/**/*.scss',
                ],
                dest: 'app/build.scss'
            }
        },
        sass: {
            dev: {
                files: {
                    'app/css/all.css': 'app/build.scss'
                }
            }
        },
        postcss: {
            options: {
                processors: [
                    // vendor prefixes rule
                    require('autoprefixer-core')({
                        browsers: ['last 2 versions']
                    })
                ]
            },
            dist: {
                src: 'app/css/all.css',
                dest: 'app/css/all.css'
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'app/css/*.css',
                        'app/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: './app' // server path
                }
            }
        }
    });

    // npm tasks
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-postcss');

    // start browsersync server and watch file changes
    grunt.registerTask('serve', ['browserSync', 'watch']);
};
